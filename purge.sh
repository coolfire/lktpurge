#!/bin/bash

iptables-save > /tmp/iptables.rules
python unsorteduniq.py /tmp/iptables.rules > /tmp/iptables.uniq.rules
iptables-restore < /tmp/iptables.uniq.rules
