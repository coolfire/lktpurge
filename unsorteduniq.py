#!/usr/bin/python

import sys

uniqList  = []

try:
  inputfile = open(sys.argv[1], 'r')

  while True:
    line = inputfile.readline().rstrip('\n')
    if (not line in uniqList) or ('COMMIT' in line):
      uniqList.append(line)

    if not line:
      break

finally:
  inputfile.close()

print('\n'.join(uniqList))
